c=2.99\cdot10^8

h=6.626\cdot10^{-34}

b=1.38\cdot10^{-23}

B\left(l,t\right)=\frac{2hc^2}{l^5\left(e^{\frac{hc}{lbt}}-1\right)}

y=\frac{B\left(x\cdot10^{-6},\left[3000,4000,5000\right]\right)}{1\cdot10^{12}}