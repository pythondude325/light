This format is to be used to represent values along a spectrum. 

Files with this format should use the .spec.json file extension.

It can be used for Solar emmission spectrum, CIE standard observer values,
standard illuminant values, transmission spectrum, or reflectance spectrum.

The spectral data files will be in JSON format and include the following fields:

metadata:
    wavelength_unit: string
        // A string denoting the unit used for wavelengths in the file.
        // Should be "meter", "m", "micrometer", "μm", "nanometer", or "nm",
        // Other SI units (or non-SI units may be supported in the future)
    data_unit: string
        // The unit for that the data is in. May be none in the case of an
        // assumed unit. (ex: "Wm^-2μm^-1")

    dimensions:
        start_wavelength: float
            // The wavelength at which the data starts
        end_wavelength: float
            // The wavelength at which the data ends
        interval: float
            // The wavelength interval between entries in the data
        length: integer
            // An integer denoting the number of elements in the data

        // The following equation should be true:
        // start_wavelength + interval * length == end_wavelength

    extra: string
        // Any extra information about the spectrum data like how it was made or
        // where it came from.

data: [float; metadata.length]
    // The spectral data