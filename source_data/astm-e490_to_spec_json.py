#!/usr/bin/env python3

import json
import numpy as np
from scipy.interpolate import interp1d

wavelength_start = 380
wavelength_end = 780
wavelength_step = 1

data = np.genfromtxt("ASTM-E490.csv", delimiter=",").transpose()

wavelengths = np.multiply(data[0], 1000) # Convert from micrometers to nanometers
powers = data[1]

interpolation = interp1d(wavelengths, powers)

output_data = interpolation(np.arange(wavelength_start, wavelength_end + 1, wavelength_step))

# Verify dimensions
if wavelength_start + (len(output_data) - 1) * wavelength_step != wavelength_end:
    raise ValueError("Dimensions do not align")

print(json.dumps({
    "metadata": {
        "wavelength_unit": "nanometer",
        "data_unit": "Wm^-2μm^-1",
        "extra": "This data comes form the ASTM E-490 solar spectra (zero air mass) standard.",
        "dimensions": {
            "start_wavelength": wavelength_start,
            "end_wavelength": wavelength_end,
            "interval": wavelength_step,
            "length": len(output_data),
        }
    },
    "data": output_data.tolist()
}))
