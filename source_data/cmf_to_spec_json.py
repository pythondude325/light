#!/usr/bin/env python3

import sys
import json

def make_observer(start, end, interval, length):
    return  {
        "metadata": {
            "wavelength_unit": "nanometer",
            "data_unit": "",
            "dimensions": {
                "start_wavelength": start,
                "end_wavelength": end,
                "interval": interval,
                "length": length
            }
        },
        "data": []
    }


input_lines = sys.stdin.readlines()

tupleize_line = lambda line: (int(line[0]), float(line[1]), float(line[2]), float(line[3]))

input_data = [tupleize_line(line.split("\t")) for line in input_lines[1:]]

start_wavelength = input_data[0][0]
end_wavelength = input_data[-1][0]
interval = input_data[1][0] - start_wavelength
length = len(input_data)

if start_wavelength + (length - 1) * interval != end_wavelength:
    raise ValueError("Interval math is wrong")


x_observer = make_observer(start_wavelength, end_wavelength, interval, length)
y_observer = make_observer(start_wavelength, end_wavelength, interval, length)
z_observer = make_observer(start_wavelength, end_wavelength, interval, length)


for data in input_data:
    x_observer["data"].append(data[1])
    y_observer["data"].append(data[2])
    z_observer["data"].append(data[3])

print(json.dumps({
    "x": x_observer,
    "y": y_observer,
    "z": z_observer
}))
    