precision mediump float;
uniform vec2 resolution;
uniform float wavelength;

uniform sampler2D x_observer_data;
uniform mat2 x_observer_transform;

uniform sampler2D y_observer_data;
uniform mat2 y_observer_transform;

uniform sampler2D z_observer_data;
uniform mat2 z_observer_transform;

uniform sampler2D sun_spectrum_data;
uniform mat2 sun_spectrum_transform;

float getData(sampler2D data, mat2 transform, float i){
	return texture2D(data, transform * vec2(i, 1.0)).x;
}

vec3 wl2XYZ(float wl){
  return vec3(
	  getData(x_observer_data, x_observer_transform, wl),
	  getData(y_observer_data, y_observer_transform, wl),
	  getData(z_observer_data, z_observer_transform, wl)
  );
}

float adj(float u){
	return (u < 0.0031308) ? (12.92 * u) : (1.055 * pow(u, 1./2.4) - 0.055);
}

vec3 XYZ2sRGB(vec3 i){
	vec3 u = mat3(
		3.2406, -0.9689, 0.0557,
		-1.5372, 1.8758, -0.2040,
		-0.4986, 0.0415, 1.0570) * i;
	return vec3(adj(u.x), adj(u.y), adj(u.z));
}

vec3 xyY2XYZ(vec3 i){
	float x=i.x, y=i.y, Y=i.z;
	return vec3(Y/y*x, Y, Y/y*(1.-x-y));
}


vec3 spectrum_to_color(sampler2D data, mat2 transform, float sky_distance){
	vec3 total_color = vec3(0.0);

	for(int wl = 380; wl <= 780; wl++){
		float diffusion_factor = pow(float(wl), -4.0) * 1e10;

		total_color += wl2XYZ(float(wl)) * getData(data, transform, float(wl)) * pow(1.0 - diffusion_factor, sky_distance) * diffusion_factor;
	}

	return total_color;
}


// In xyY
// sRGB red: 0.6400, 0.3300, 0.2126
// sRGB green: 0.3, 0.6, 0.7152
// sRGB blue: 0.15, 0.06, 0.0722
// sRGB white: 0.3127, 0.3290, 1.

vec3 clamp_color(vec3 c){
	if(c.r >= c.g && c.r >= c.b && c.r > 1.0){
		return c / c.r;
	} else if(c.g >= c.r && c.g >= c.b && c.g > 1.0){
		return c / c.g;
	} else if(c.b >= c.r && c.b >= c.g && c.b > 1.0){
		return c / c.b;
	} else {
		return c;
	}
}

void main( void ) {
	vec2 pos = ( gl_FragCoord.xy / resolution.y );
		
	vec3 i = spectrum_to_color(sun_spectrum_data, sun_spectrum_transform, pos.x * 20.0) / wavelength;

	vec3 color = XYZ2sRGB(i);

	// if(pos.x > 1.2){
	// 	color = vec3(0.5);
	// }

	gl_FragColor = vec4( clamp_color(color) , 1.0 );
}