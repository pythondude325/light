import CanvasManager from "./canvasManager.js"
import inputs from "./inputs.js"
import load_spectral_data from "./load_spec_data.js"

async function main() {
    const container = document.getElementById("canvas-container")

    const canvasManager = new CanvasManager(container)
    window.canvasManager = canvasManager
    canvasManager.renderScale = 2
    canvasManager.update()

    const sidebar = document.getElementById("sidebar")

    let render_scale_select = new inputs.ResponsiveSelect("Render Scale", ["0.5", "1", "2", "4", "8"], "2", (v) => {
        canvasManager.renderScale = parseFloat(v)
        canvasManager.update()
    })
    sidebar.appendChild(render_scale_select.dom_element)


    window.addEventListener("resize", () => {
        canvasManager.update()
    })

    let observers = await fetch("source_data/cmf.spec.json").then(r => r.json())

    let solar_data = await fetch("source_data/astm-e490.spec.json").then(r => r.json())

    const drawTriangle = canvasManager.regl({
        frag: await fetch("/frag.glsl").then(r => r.text()),

        vert: `
    precision mediump float;
    attribute vec2 position;
    void main() {
      gl_Position = vec4(position, 0, 1);
    }`,

        attributes: {
            position: canvasManager.regl.buffer([[-1, -1], [1, -1], [1, 1], [-1, 1], [1, 1], [-1, -1]])
        },
        count: 6,

        uniforms: {
            resolution: (context) => [context.drawingBufferWidth, context.drawingBufferHeight],
            // ... await load_spectral_data(canvasManager, "xyz_spectrum", "/data/spectrum_xyz.json"),
            ...load_spectral_data(canvasManager, "x_observer", observers.x),
            ...load_spectral_data(canvasManager, "y_observer", observers.y),
            ...load_spectral_data(canvasManager, "z_observer", observers.z),
            ...load_spectral_data(canvasManager, "sun_spectrum", solar_data),
            wavelength: canvasManager.regl.prop("wavelength")
        }
    })

    /*// regl.frame() wraps requestAnimationFrame and also handles viewport changes
    canvasManager.regl.frame(() => {
        drawTriangle({})
    })*/

    window.draw = (wl) => drawTriangle({ wavelength: wl })
}

window.addEventListener("DOMContentLoaded", main)