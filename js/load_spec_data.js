function load_spectral_data(cm, name, spec_data) {
    // spec_data should be an object in the .spec.json format

    const dimensions = spec_data.metadata["dimensions"]

    const S = dimensions["start_wavelength"]
    const E = dimensions["end_wavelength"]
    const I = dimensions["interval"]
    const L = dimensions["length"]

    // Verify length
    if (spec_data.data.length != L) {
        throw new Error(`Metadata length (${L}) does not match data length. (${spec_data.data.length})`)
    }

    // Veryify dimensions
    if (S + (L - 1) * I != E) {
        throw new Error(`Metadata dimensions did not align. (S: ${S}, E: ${E}, I: ${I}, L: ${L})`)
    }

    return {
        [name + "_data"]: cm.regl.texture({
            height: 1,
            width: spec_data.data.length,
            data: spec_data.data,
            format: "luminance",
            type: "float"
        }),
        [name + "_transform"]: [1/(E-S), 0, -S/(E-S), 1]
    }
}


export default load_spectral_data