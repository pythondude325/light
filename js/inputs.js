class ResponsiveSlider {
    constructor(name, init_value, min, max, callback){
        this.dom_element = document.createElement("div")
        this.name_label = document.createElement("div")
        this.name_label.appendChild(new Text(name + ": "))
        this.value_label = document.createElement("span")
        this.name_label.appendChild(this.value_label)
        this.dom_element.appendChild(this.name_label)
        this.slider = document.createElement("input")
        this.slider.type = "range"
        this.slider.min = min
        this.slider.max = max
        this.slider.step = 0.01
        this.dom_element.appendChild(document.createElement("div").appendChild(this.slider))

        this.callback = callback

        this.slider.addEventListener("input", () => this.onInput())

        this.slider.value = init_value
        this.onInput()
    }

    onInput(){
        let v = this.value
        this.value_label.innerText = v.toFixed(0)

        this.callback(v)
    }

    get value(){
        return parseFloat(this.slider.value)
    }
}

class ExpResponsiveSlider extends ResponsiveSlider {
    constructor(){
        super(...arguments)
    }

    onInput(){
        let v = Math.exp(this.value)
        this.value_label.innerText = v.toFixed(0)

        this.callback(v)
    }
}

class ResponsiveSelect {
    constructor(label, choices, init_value, callback){
        this.dom_element = document.createElement("div")
        this.dom_element.appendChild(new Text(label + ": "))
        this.select = document.createElement("select")
        for (let choice of choices) {
            let value, text
            if(typeof choice == "object"){
                value = choice.value
                text = choice.text
            } else if(typeof choice == "string"){
                value = choice
                text = choice
            } else {
                throw new TypeError("Choices must be an array of strings or objects")
            }

            let option = document.createElement("option")
            option.value = value
            option.innerText = text
            this.select.appendChild(option)
        }
        this.dom_element.appendChild(this.select)

        this.callback = callback

        this.select.value = init_value

        this.select.addEventListener("input", () => this.onInput())
    }

    onInput(){
        this.callback(this.value)
    }

    get value(){
        return this.select.value
    }
}

export default { ResponsiveSlider, ExpResponsiveSlider, ResponsiveSelect }